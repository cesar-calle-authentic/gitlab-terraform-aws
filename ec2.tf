resource "aws_instance" "ec2-instance-example" {
  ami           = "ami-0ef0c69399dbb5f3f"
  instance_type = "t2.micro"

  tags = {
    Name = var.name
  }
}
